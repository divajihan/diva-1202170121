<?php
session_start();
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Home | Edit Profile</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

    <nav class="navbar navbar-light">
      <style type="text/css">
        .navbar{
          padding-top: 10px;
          padding-bottom: 0px;
        }
      </style>
      <a class="navbar-brand" href="home.php">
        <img src="EAD.png" height="30" alt="">
      </a>
      <ul class="nav justify-content-end">
        <li class="nav-item">
          <i class="fa fa-shopping-cart"></i>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <a class="nav-link dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $_SESSION["username"]; ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="cart.php">Cart</a>
              <a class="dropdown-item" href="profile.php">Edit Profile</a>
              <a class="dropdown-item" href="logout.php">Logout</a>
            </div>
          </div>
        </li>
      </ul>
    </nav>
    <hr>

    <form class="profile" action="fungsi.php" method="post">
      <style type="text/css">
  	    .profile{
          padding-top: 20px;
  				padding-left: 300px;
  				padding-right: 300px;
  			}
  	  </style>
      <center><h3>Profile</h3></center>
      <br>
      <div class="form-group row">
        <label for="profileemail" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="profileemail" name="profileemail" placeholder="Email" value="<?php echo $_SESSION["email"]; ?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label for="profileusername" class="col-sm-2 col-form-label">Username</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="profileusername" name="profileusername" placeholder="Username">
        </div>
      </div>
      <div class="form-group row">
        <label for="profilenumber" class="col-sm-2 col-form-label">Mobile Number</label>
        <div class="col-sm-10">
          <input type="number" class="form-control" id="profilenumber" name="profilenumber" placeholder="Mobile Number">
        </div>
      </div>
      <hr>
      <div class="form-group row">
        <label for="profilenpassword" class="col-sm-2 col-form-label">New Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="profilenpassword" name="profilenpassword" placeholder="New Password">
        </div>
      </div>
      <div class="form-group row">
        <label for="profileconfpassword" class="col-sm-2 col-form-label">Confirm Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="profileconfpassword" name="profileconfpassword" placeholder="Confirm Password">
        </div>
      </div>
      <br>
      <center>
      <div class="form-group row">
        <div class="col-sm-12">
          <input type="submit" class="btn btn-primary btn-lg btn-block" name="editprofile" value="Save">
          <button type="button" class="btn btn-link" href="home.php">Cancel</button>
        </div>
      </div>
      </center>
    </form>

    <br>

    <!-- Footer -->
    <footer class="page-footer">
      <!-- Copyright -->
      <div class="footer-copyright text-center py-3">© EAD STORE</div>
      <!-- Copyright -->
    </footer>
    <!-- Footer -->

  </body>
</html>

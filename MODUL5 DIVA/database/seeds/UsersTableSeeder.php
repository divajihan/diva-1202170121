<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'test',
          'email' => 'test@gmail.com',
          'password' => bcrypt('test'),
          'avatar' => 'images/profile.png'
        ]);
    }
}

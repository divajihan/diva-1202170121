<?php
include 'koneksi.php';
session_start();
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Home | Cart</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <nav class="navbar navbar-light">
      <style type="text/css">
        .navbar{
          padding-top: 10px;
          padding-bottom: 0px;
        }
      </style>
      <a class="navbar-brand" href="home.php">
        <img src="EAD.png" height="30" alt="">
      </a>
      <ul class="nav justify-content-end">
        <li class="nav-item">
          <i class="fa fa-shopping-cart"></i>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <a class="nav-link dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php echo $_SESSION["username"]; ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="cart.php">Cart</a>
              <a class="dropdown-item" href="profile.php">Edit Profile</a>
              <a class="dropdown-item" href="logout.php">Logout</a>
            </div>
          </div>
        </li>
      </ul>
    </nav>
    <hr>

  <div class="table">
    <style type="text/css">
  		.table{
  			padding-top: 40px;
  			padding-left: 300px;
  			padding-right: 300px;
  		}
  	</style>
    <table class="table table-borderless">
      <style type="text/css">
        .table{
          padding-top: 20px;
          padding-left: 300px;
          padding-right: 300px;
        }
      </style>
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Product</th>
          <th scope="col">Price</th>
          <th scope="col"> </th>
        </tr>
      </thead>
      <tbody>
        <?php
          $id = $_SESSION["id"];
          $query = "SELECT * from cart where user_id='$id'";
          $select = mysqli_query($conn,$query);
          $count = 1;
          $total = 0;
          while ($row=mysqli_fetch_array($select)) {
        ?>
        <tr>
          <th><?= $count ?></th>
          <td><?= $row['product'] ?></td>
          <td><?= $row['price'] ?></td>
          <td></td>
        </tr>

        <?php
          $count++;
          $total=$total+$row['price'];
          }
        ?>
        <tr>
          <th colspan="2"><center>TOTAL</center></th>
          <td><?= $total?></td>
          <td><a href="hapus.php?id=<?= $id ?>"><i class="fa fa-ban"></i></a></td>
        </tr>

      </tbody>
    </table>
  </div>

    <!-- Footer -->
    <footer class="page-footer">
      <!-- Copyright -->
      <div class="footer-copyright text-center py-3">© EAD STORE</div>
      <!-- Copyright -->
    </footer>
    <!-- Footer -->

  </body>
</html>

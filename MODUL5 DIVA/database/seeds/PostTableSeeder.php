<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('post')->insert([
        'user_id' => '1',
        'caption' => 'Hello, World!',
        'image' => 'images/EAD.png',
      ]);
    }
}

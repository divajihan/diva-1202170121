<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Home | Welcome!</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
	<?php
	if (isset($_SESSION["login"])) {
	?>
			<nav class="navbar navbar-light">
				<style type="text/css">
					.navbar{
						padding-top: 10px;
						padding-bottom: 0px;
					}
				</style>
				<a class="navbar-brand" href="#">
					<img src="EAD.png" height="30" alt="">
				</a>
				<ul class="nav justify-content-end">
					<li class="nav-item">
						<i class="fa fa-shopping-cart"></i>
					</li>
					<li class="nav-item">
						<div class="dropdown">
							<a class="nav-link dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<?php echo $_SESSION["username"]; ?>
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="cart.php">Cart</a>
								<a class="dropdown-item" href="profile.php">Edit Profile</a>
								<a class="dropdown-item" href="logout.php">Logout</a>
							</div>
						</div>
					</li>
				</ul>
			</nav>
			<hr>
	<?php
	} else {
	?>
		<nav class="navbar navbar-light">
			<style type="text/css">
		    .navbar{
					padding-top: 10px;
					padding-bottom: 0px;
				}
		  </style>
		  <a class="navbar-brand" href="#">
		    <img src="EAD.png" height="30" alt="">
		  </a>
			<ul class="nav justify-content-end">
				<li class="nav-item">
					<a class="nav-link active" href="#" data-toggle="modal" data-target="#login">Login</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#" data-toggle="modal" data-target="#register">Register</a>
				</li>
			</ul>
		</nav>
		<hr>
	<?php
	}
	?>

<!-- Modal Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="login">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			<div class="modal-body">
				<form action="fungsi.php" method="post">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Email address</label>
				    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
				  </div>
					<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <input type="submit" name="login" class="btn btn-primary" value="Login">
		      </div>
				</form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Register -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="register">Register</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			<div class="modal-body">
				<form action="fungsi.php" method="post">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Email address</label>
				    <input type="email" class="form-control" id="nemail" name="nemail" aria-describedby="emailHelp" placeholder="Enter email">
				  </div>
					<div class="form-group">
				    <label for="exampleInputUsername1">Username</label>
				    <input type="text" class="form-control" id="nusername" name="nusername" placeholder="Enter username">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" class="form-control" id="npassword" name="npassword" placeholder="Password">
				  </div>
					<div class="form-group">
				    <label for="exampleInputConfPassword1">Confirm Password</label>
				    <input type="password" class="form-control" id="nconfpassword" name="nconfpassword" placeholder="Confirm Password">
				  </div>
					<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <input type="submit" name="register" class="btn btn-primary" value="Register">
		      </div>
				</form>
      </div>
    </div>
  </div>
</div>

	<div class="welcome">
		<style type="text/css">
	    .welcome{
				padding-top: 20px;
				padding-left: 300px;
				padding-right: 300px;
			}
	  </style>
		<div class="card bg-white text-dark">
		  <img src="background.jpg" class="card-img" alt="background" height="200px">
		  <div class="card-img-overlay">
				<br>
		    <h1 class="card-title">Hello Coders</h1>
		    <p class="card-text">Welcome to our store, please take a look for the products you might buy</p>
		  </div>
		</div>
	</div>

<div class="option card-deck row">
	<style type="text/css">
		.option{
			padding-top: 40px;
			padding-left: 300px;
			padding-right: 300px;
		}
	</style>

	<div class="card col-md-4" style="width: 18rem" >
	  <img src="Web.png" class="card-img-top" alt="web">
	  <div class="card-body">
	    <h6 class="card-title">Learning Basic Web Programming</h6>
			<h6 class="harga1">Rp 210.000,-</h6>
	    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			<hr>
			<form action="fungsi.php" method="post">
			<center><input type="submit" name="Beli1" class="btn btn-primary" value="Buy"></center>
	  </div>
	</div>
	<div class="card col-md-4" style="width: 18rem;">
	  <img src="Java1.png" class="card-img-top" alt="java">
	  <div class="card-body">
	    <h6 class="card-title">Starting Programming in Java</h6>
			<h6 class="harga2">Rp 150.000,-</h6>
	    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			<hr>
	    <center><input type="submit" name="Beli2" class="btn btn-primary" value="Buy"></center>
	  </div>
	</div>

	<div class="card col-md-4" style="width: 18rem;">
	  <img src="Phyton.png" class="card-img-top" alt="phyton">
	  <div class="card-body">
	    <h6 class="card-title">Starting Programming in Phyton</h6>
			<h6 class="harga3">Rp 200.000,-</h6>
	    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			<hr>
	    <center><input type="submit" name="Beli3" class="btn btn-primary" value="Buy"></center>
	  </div>
	</div>
	</form>
</div>

<br>

<!-- Footer -->
<footer class="page-footer">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© EAD STORE</div>
  <!-- Copyright -->
</footer>
<!-- Footer -->

</body>
</html>

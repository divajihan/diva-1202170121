@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @foreach($posts as $data)
            <div class="card">
                <div class="card-header">
                  <img src="{{asset($data->user->avatar)}}" alt="a" class="rounded-circle" width="5%" height="40%">&ensp;
                  {{$data->user->name}}
                </div>

                <div class="card-body">
                  <center><img src="/storage/{{ $data->image }}" alt="b" class="card-img-too" width="300" height="300"></center> <br>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

                <div class="card-footer">
                  {{$data->user->email}} <br>
                  {{$data->caption}}
                </div>

            </div>
            <br>
          @endforeach
        </div>
    </div>
</div>
@endsection

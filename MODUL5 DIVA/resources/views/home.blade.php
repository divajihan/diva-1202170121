@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @foreach($a as $data)
            <div class="card">
                <div class="card-header">
                  <img src="{{asset($data->avatar)}}" alt="a" class="rounded-circle" width="5%" height="40%">&ensp;
                  {{$data->name}}
                </div>

                <div class="card-body">
                  <img src="{{asset($data->image)}}" alt="b" class="card-img-too" width="650" height="450"> <br>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

                <div class="card-footer">
                  {{$data->email}} <br>
                  {{$data->caption}}
                </div>

            </div>
          @endforeach
        </div>
    </div>
</div>
@endsection
